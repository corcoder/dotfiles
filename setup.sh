rm ~/.zshrc
ln -s ~/Setup/dotfiles/.zshrc ~/.zshrc

rm -r ~/.config/nvim/autoload/plug.vim
ln -s ~/Setup/dotfiles/nvim/autoload/plug.vim ~/.config/nvim/autoload/plug.vim

rm ~/.config/nvim/init.vim
ln -s ~/Setup/dotfiles/nvim/init.vim ~/.config/nvim/init.vim

rm ~/.tmux.conf
ln -s ~/Setup/dotfiles/.tmux.conf ~/.tmux.conf

